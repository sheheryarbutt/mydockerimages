create database phone;

use phone;

create table numbers (
 name varchar(50),
 phone varchar(50)
);

insert into numbers VALUES("Sheheryar Butt", "123-4567");

insert into numbers VALUES("Random guy", "987-5433");

insert into numbers VALUES("TEST", "123-1241");
