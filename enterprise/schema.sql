-- This creates the database with intial database

create database phone;

use phone;

create table numbers(
  name varchar(50),
  number varchar(30)
);

insert into numbers values('Sheheryar Butt','55-123');
insert into numbers values('firstname_1 lastname_1','55-152223');
insert into numbers values('firstname_2 lastname_2','55-12443');
insert into numbers values('firstname_3 lastname_3','55-12348');
